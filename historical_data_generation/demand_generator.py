import matplotlib.pyplot as plt
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from tools import node_parser
import numpy as np

def gen_demand(type_of_day, tourism, size):
    if type_of_day == 1:
        multi = 1
    elif type_of_day == 2:
        multi = 1.5
    else:
        multi = 3

    demand = (size+size*(tourism/3))*multi
    return demand



def demand_for_shops(type_of_day):
    node_data = node_parser.parse_node_parameters("data/out/node_parameters.txt")
    node_data_numerical = []

    size = {
        "very_small": 1,
        "small": 2,
        "medium": 3,
        "big": 4,
        "very_big": 5
    }

    tourism = {
        "low_tourism": 1,
        "medium_tourism": 2,
        "high_tourism": 3
    }

    for node in node_data:
        node_data_numerical.append((tourism.get(node[2]), size.get(node[0])))

    shops_matrix = np.zeros((len(node_data),1), np.int64)
        
    for i in range(len(node_data_numerical)):
        node = node_data_numerical[i]
        demand = gen_demand(type_of_day, node[0], node[1])
        shops_matrix[i] = abs(int(round(demand)))

    return shops_matrix