import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import numpy as np
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

from historical_data_generation import demand_generator
from historical_data_generation import matrix_generator
from tools import time_generator

import matplotlib.pyplot as plt

import optuna

def create_data_model(type_of_day, n, n_car_type_1, n_car_type_2, n_car_type_3):
    """Stores the data for the problem."""
    data = {}
    travel_time_matrix = matrix_generator.generate_time_matrix("data/in/nodes.json", n)

    data['travel_time_matrix'] = travel_time_matrix
    data['time_windows'] = time_generator.generate_time_windows(n)
    # data['num_vehicles'] = 3 * n + n_car_type_1 + n_car_type_2 + n_car_type_3
    data['num_vehicles'] = n
    data['depot'] = 0
    data['demands'] = demand_generator.demand_for_shops(type_of_day).T[0][0:n+1] #???
    data['demands'][0]=0                    #THIS NEEDS TO BE FIXED
    data['vehicle_capacities'] = np.concatenate([(np.ones((1,n_car_type_1), np.int64)[0]*64), (np.ones((1,n_car_type_2), np.int64)[0]*32), (np.ones((1,n_car_type_3), np.int64)[0]*16), (np.ones((1,int((n-n_car_type_1-n_car_type_2-n_car_type_3)/3)), np.int64)[0]*64), (np.ones((1,int((n-n_car_type_1-n_car_type_2-n_car_type_3)/3)), np.int64)[0]*32), (np.ones((1,(n-n_car_type_1-n_car_type_2-n_car_type_3)-2*int((n-n_car_type_1-n_car_type_2-n_car_type_3)/3)), np.int64)[0]*16)])
    # data['vehicle_capacities'] = np.ones((1,n), np.int64)[0]*32
    #print(data['vehicle_capacities'])
    return data


def return_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()}')
    time_dimension = routing.GetDimensionOrDie('Time')
    total_time = 0
    total_distance = 0
    total_load = 0
    needed_vehicles = 0
    type_1 = 0
    type_2 = 0
    type_3 = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        route_load = 0
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            previous_index = index
            index = solution.Value(routing.NextVar(index))
        time_var = time_dimension.CumulVar(index)
        total_load += route_load
        total_time += solution.Min(time_var)
        if total_load == 0:
            continue
        if(solution.Min(time_var)==0):
            continue
        needed_vehicles += 1
        if data['vehicle_capacities'][vehicle_id] == 64:
            type_1 += 1
        elif data['vehicle_capacities'][vehicle_id] == 32:
            type_2 += 1
        else:
            type_3 += 1
    return (needed_vehicles, type_1, type_2, type_3)


def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()}')
    time_dimension = routing.GetDimensionOrDie('Time')
    total_time = 0
    total_distance = 0
    total_load = 0
    needed_vehicles = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(needed_vehicles+1)
        route_load = 0
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += '\tNode: {0:3}\tDemand: {1:2}\t'.format(node_index, data['demands'][node_index])
            plan_output += 'Time: {0:3} - {1:3}\t\tOpen hours: {2:3} - {3:3}\n'.format(
                solution.Min(time_var),
                solution.Max(time_var),
                data['time_windows'][node_index][0],
                data['time_windows'][node_index][1])
            previous_index = index
            index = solution.Value(routing.NextVar(index))
        time_var = time_dimension.CumulVar(index)
        plan_output += '\tNode: {0:3}\tDemand: {1:2}\t'.format(manager.IndexToNode(index), data['demands'][manager.IndexToNode(index)])
        plan_output += 'Time: {0:3} - {1:3}\t\tOpen hours: {2:3} - {3:3}\n'.format(manager.IndexToNode(index),
                                                    solution.Min(time_var),
                                                    solution.Max(time_var),
                                                    data['time_windows'][0][1], #???
                                                    data['time_windows'][0][0])
        plan_output += 'Load of the route: {}\n'.format(route_load)
        plan_output += 'Time of the route: {}\n'.format(
            solution.Min(time_var))
        if(solution.Min(time_var)==0):
            continue
        print(plan_output)
        needed_vehicles += 1
        total_time += solution.Min(time_var)
        total_load += route_load
    print('=============================================')
    print('Vehicles needed: {}'.format(needed_vehicles))
    print('Total time of all routes: {}'.format(total_time))
    print('Total load of all routes: {}'.format(total_load))
    print('=============================================')
    return(needed_vehicles)

def solve_date(type_of_day, n, car_typ1, n_car_type_2, n_car_type_3, print_switch=False):
    def time_callback(from_index, to_index):
        """Returns the travel time between the two nodes."""
        # Convert from routing variable Index to time matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['travel_time_matrix'][from_node][to_node]

    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]

    data = create_data_model(type_of_day, n, car_typ1, n_car_type_2, n_car_type_3)
    # print(data['time_windows'])
    # print(data['travel_time_matrix'])
    manager = pywrapcp.RoutingIndexManager(len(data['travel_time_matrix']), data['num_vehicles'], data['depot'])
    routing = pywrapcp.RoutingModel(manager)

    ###


    transit_callback_index = routing.RegisterTransitCallback(time_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    time = 'Time'
    routing.AddDimension(
        transit_callback_index,
        7*6,  # allow waiting time
        23*6,  # maximum time per vehicle
        False,  # Don't force start cumul to zero.
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    for i in range(n):
        time_dimension.SetSpanUpperBoundForVehicle(9*6, i)
    # Add time window constraints for each location except depot.
    for location_idx, time_window in enumerate(data['time_windows']):
        if location_idx == data['depot']:
            continue
        index = manager.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])


    # Add time window constraints for each vehicle start node.
    depot_idx = data['depot']
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        time_dimension.CumulVar(index).SetRange(
            data['time_windows'][depot_idx][0],
            data['time_windows'][depot_idx][1])
    for i in range(data['num_vehicles']):
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.Start(i)))
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.End(i)))


    ###



    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,  # start cumul to zero
        'Capacity')

    ###


    for i in range(data['num_vehicles']):
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.Start(i)))
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.End(i)))

    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
            routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
    search_parameters.local_search_metaheuristic = (
            routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    search_parameters.time_limit.FromSeconds(1)

    solution = routing.SolveWithParameters(search_parameters)

    if solution:
        if print_switch:
            result = print_solution(data, manager, routing, solution)
        else:
            result = return_solution(data, manager, routing, solution)

    else:
        print('No solution found !')
        result = 10000

    return(result)

#print(solve_date(1, 100, 1, 1, 1, False))
#print(solve_date(2, 100, 1, 1, 1, False))
#print(solve_date(3, 100, 1, 1, 1, False))

def train_solver_optuna(trial):
    n_type_1 = trial.suggest_int('n_type_1', 0, 30)
    n_type_2 = trial.suggest_int('n_type_2', 0, 30)
    n_type_3 = trial.suggest_int('n_type_3', 0, 30)

    day_1_type_1, day_1_type_2, day_1_type_3 = (1, 0, 25)    # already solved dates to run this faster
    day_2_type_1, day_2_type_2, day_2_type_3 = (0, 4, 32)
    day_3_type_1, day_3_type_2, day_3_type_3 = (0, 25, 32)
    daily_cost = n_type_1*1000 + n_type_2*500 + n_type_3*250
    day_1_cost = 0
    if(day_1_type_1-n_type_1 > 0):
        day_1_cost = day_1_cost + (day_1_type_1-n_type_1)*5000
    if(day_1_type_2-n_type_2 > 0):
        day_1_cost = day_1_cost + (day_1_type_2-n_type_2)*2500
    if(day_1_type_3-n_type_3 > 0):
        day_1_cost = day_1_cost + (day_1_type_3-n_type_3)*1250
    day_2_cost = 0
    if(day_2_type_1-n_type_1 > 0):
        day_2_cost = day_2_cost + (day_2_type_1-n_type_1)*5000
    if(day_2_type_2-n_type_2 > 0):
        day_2_cost = day_2_cost + (day_2_type_2-n_type_2)*2500
    if(day_2_type_3-n_type_3 > 0):
        day_2_cost = day_2_cost + (day_2_type_3-n_type_3)*1250
    day_3_cost = 0
    if(day_3_type_1-n_type_1 > 0):
        day_3_cost = day_3_cost + (day_3_type_1-n_type_1)*5000
    if(day_3_type_2-n_type_2 > 0):
        day_3_cost = day_3_cost + (day_3_type_2-n_type_2)*2500
    if(day_3_type_3-n_type_3 > 0):
        day_3_cost = day_3_cost + (day_3_type_3-n_type_3)*1250

    cost = 30 * daily_cost + 8 * day_1_cost + 20 * day_2_cost + 2 * day_3_cost

    return cost

study1 = optuna.create_study(direction='minimize')
study1.optimize(train_solver_optuna, n_trials=10)
study2 = optuna.create_study(direction='minimize') 
study2.optimize(train_solver_optuna, n_trials=10)
study3 = optuna.create_study(direction='minimize')
study3.optimize(train_solver_optuna, n_trials=10)
study4 = optuna.create_study(direction='minimize')
study4.optimize(train_solver_optuna, n_trials=10)
study5 = optuna.create_study(direction='minimize')
study5.optimize(train_solver_optuna, n_trials=10)

plt.scatter(y=study1.trials_dataframe()["params_n_type_1"], x=list(range(1, 11)))
plt.scatter(y=study2.trials_dataframe()["params_n_type_1"], x=list(range(1, 11)))
plt.scatter(y=study3.trials_dataframe()["params_n_type_1"], x=list(range(1, 11)))
plt.scatter(y=study4.trials_dataframe()["params_n_type_1"], x=list(range(1, 11)))
plt.scatter(y=study5.trials_dataframe()["params_n_type_1"], x=list(range(1, 11)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 1 cars for 10 tries of 5 optimizer runs")
plt.show()

plt.scatter(y=study1.trials_dataframe()["params_n_type_2"], x=list(range(1, 11)))
plt.scatter(y=study2.trials_dataframe()["params_n_type_2"], x=list(range(1, 11)))
plt.scatter(y=study3.trials_dataframe()["params_n_type_2"], x=list(range(1, 11)))
plt.scatter(y=study4.trials_dataframe()["params_n_type_2"], x=list(range(1, 11)))
plt.scatter(y=study5.trials_dataframe()["params_n_type_2"], x=list(range(1, 11)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 2 cars for 10 tries of 5 optimizer runs")
plt.show()

plt.scatter(y=study1.trials_dataframe()["params_n_type_3"], x=list(range(1, 11)))
plt.scatter(y=study2.trials_dataframe()["params_n_type_3"], x=list(range(1, 11)))
plt.scatter(y=study3.trials_dataframe()["params_n_type_3"], x=list(range(1, 11)))
plt.scatter(y=study4.trials_dataframe()["params_n_type_3"], x=list(range(1, 11)))
plt.scatter(y=study5.trials_dataframe()["params_n_type_3"], x=list(range(1, 11)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 3 cars for 10 tries of 5 optimizer runs")
plt.show()

study = optuna.create_study(direction='minimize')
study.optimize(train_solver_optuna, n_trials=200)

plt.scatter(y=study.trials_dataframe()["params_n_type_1"], x=list(range(1, 201)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 1 cars for 200 tries")
plt.show()

plt.scatter(y=study.trials_dataframe()["params_n_type_2"], x=list(range(1, 201)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 2 cars for 200 tries")
plt.show()

plt.scatter(y=study.trials_dataframe()["params_n_type_3"], x=list(range(1, 201)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("Type 3 cars for 200 tries")
plt.show()

plt.scatter(y=study.trials_dataframe()["params_n_type_1"], x=list(range(1, 201)))
plt.scatter(y=study.trials_dataframe()["params_n_type_2"], x=list(range(1, 201)))
plt.scatter(y=study.trials_dataframe()["params_n_type_3"], x=list(range(1, 201)))
plt.xlabel("try")
plt.ylabel("number of cars")
plt.title("All types of cars for 200 tries")
plt.show()

plt.plot(study.trials_dataframe()["value"])
plt.xlabel("try")
plt.ylabel("cost")
plt.title("Cost for 200 tries")
plt.show()

min_value_list = []
min_value = study.trials_dataframe()["value"][0]
print(min_value)
for i in study.trials_dataframe()["value"]:
    if i < min_value:
        min_value_list.append(i)
        min_value = i
    else:
        min_value_list.append(min_value)

plt.plot(min_value_list)
plt.xlabel("try")
plt.ylabel("cost")
plt.title("Minimal cost for 200 tries")
plt.show()

# To get the dictionary of parameter name and parameter values:
print("Return a dictionary of parameter name and parameter values:",study.best_params)

# To get the best observed value of the objective function:
print("Return the best observed value of the objective function:",study.best_value)
